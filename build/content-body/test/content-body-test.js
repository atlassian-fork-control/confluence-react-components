'use strict';

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _sinon = require('sinon');

var _sinon2 = _interopRequireDefault(_sinon);

var _sinonChai = require('sinon-chai');

var _sinonChai2 = _interopRequireDefault(_sinonChai);

var _enzyme = require('enzyme');

var _chai = require('chai');

var _chai2 = _interopRequireDefault(_chai);

var _chaiEnzyme = require('chai-enzyme');

var _chaiEnzyme2 = _interopRequireDefault(_chaiEnzyme);

var _contentBody = require('../content-body');

var _contentBody2 = _interopRequireDefault(_contentBody);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

_chai2.default.use((0, _chaiEnzyme2.default)());
_chai2.default.use(_sinonChai2.default);

describe('ContentBody', function () {
    var content = void 0;
    var wrapper = void 0;
    var onContentLoadedSpy = void 0;
    var baseUrl = 'www.atlassian.com';
    var contextPath = '';

    beforeEach(function () {
        onContentLoadedSpy = _sinon2.default.spy();
        content = {
            id: '123',
            body: '<html></html>',
            cssDependencies: '',
            jsDependencies: [],
            isSPAFriendly: true
        };
    });

    afterEach(function () {
        onContentLoadedSpy.reset();
    });

    describe('componentDidMount', function () {
        it('Should call onContentLoaded when it is SPA friendly', function () {
            content.isSPAFriendly = true;
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(_contentBody2.default, { content: content, baseUrl: baseUrl, onContentLoaded: onContentLoadedSpy }));

            var instance = wrapper.instance();
            instance.componentDidMount();

            (0, _chai.expect)(onContentLoadedSpy).to.have.been.called;
        });

        it('Should not call onContentLoaded when it is not SPA friendly', function () {
            content.isSPAFriendly = false;
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(_contentBody2.default, { content: content, baseUrl: baseUrl, onContentLoaded: onContentLoadedSpy }));

            var instance = wrapper.instance();
            instance.componentDidMount();

            (0, _chai.expect)(onContentLoadedSpy).to.not.have.been.called;
        });
    });

    describe('componentDidUpdate', function () {
        it('Should call onContentLoaded when it is SPA friendly', function () {
            content.isSPAFriendly = true;
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(_contentBody2.default, { content: content, baseUrl: baseUrl, onContentLoaded: onContentLoadedSpy }));

            var instance = wrapper.instance();
            instance.componentDidUpdate();

            (0, _chai.expect)(onContentLoadedSpy).to.have.been.called;
        });

        it('Should not call onContentLoaded when it is not SPA friendly', function () {
            content.isSPAFriendly = false;
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(_contentBody2.default, { content: content, baseUrl: baseUrl, onContentLoaded: onContentLoadedSpy }));

            var instance = wrapper.instance();
            instance.componentDidUpdate();

            (0, _chai.expect)(onContentLoadedSpy).to.not.have.been.called;
        });
    });

    describe('Called with content that is SPA friendly', function () {
        beforeEach(function () {
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(_contentBody2.default, { content: content, baseUrl: baseUrl, onContentLoaded: onContentLoadedSpy }));
        });

        it('Should render ContentBodyRest', function () {
            (0, _chai.expect)(wrapper.is('ContentBodyRest')).to.equal(true);
            (0, _chai.expect)(wrapper).to.not.have.prop('onContentLoaded');
        });

        it('Should pass on the content to ContentBodyRest', function () {
            (0, _chai.expect)(wrapper).to.have.prop('content').deep.equal(content);
        });
    });

    describe('Called with content that is not SPA friendly', function () {
        beforeEach(function () {
            content.isSPAFriendly = false;
            wrapper = (0, _enzyme.shallow)(_react2.default.createElement(_contentBody2.default, { content: content, baseUrl: baseUrl, contextPath: contextPath, onContentLoaded: onContentLoadedSpy }));
        });

        it('Should render ContentBodyIframe', function () {
            (0, _chai.expect)(wrapper.is('ContentBodyIframe')).to.equal(true);
            (0, _chai.expect)(wrapper).to.have.prop('onContentLoaded').equal(onContentLoadedSpy);
        });

        it('Should pass on the content to ContentBodyIframe', function () {
            (0, _chai.expect)(wrapper).to.have.prop('content').deep.equal(content);
        });

        it('Should pass on the baseUrl to ContentBodyIframe', function () {
            (0, _chai.expect)(wrapper).to.have.prop('baseUrl').equal(baseUrl);
        });

        it('Should pass on the contextPath to ContentBodyIframe', function () {
            (0, _chai.expect)(wrapper).to.have.prop('contextPath').equal(contextPath);
        });
    });
});