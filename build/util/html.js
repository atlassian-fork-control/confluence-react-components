'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.convertRelativeToAbsolute = convertRelativeToAbsolute;
function convertRelativeToAbsolute(html, baseUrl) {
    if (baseUrl.charAt(baseUrl.length - 1) !== '/') {
        baseUrl = baseUrl + '/';
    }
    // TODO: Find a better way to do this!
    html = html.replace(/ src="(?!\/\/)(?!http:\/\/)(?!https:\/\/)\/?([^"]+)"/g, ' src="' + baseUrl + '$1"');
    html = html.replace(/ href="(?!\/\/)(?!http:\/\/)(?!https:\/\/)\/?([^"]+)"/g, ' href="' + baseUrl + '$1"');
    return html;
}