'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _uuid = require('../util/uuid');

var _uuid2 = _interopRequireDefault(_uuid);

var _window = require('../facades/window');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var DEFAULT_CONTEXT_PATH = '/confluence';

var ContentBodyIframe = function (_Component) {
    _inherits(ContentBodyIframe, _Component);

    function ContentBodyIframe() {
        _classCallCheck(this, ContentBodyIframe);

        var _this = _possibleConstructorReturn(this, Object.getPrototypeOf(ContentBodyIframe).call(this));

        _this.iframeId = (0, _uuid2.default)();

        _this.state = {
            height: '1000px'
        };
        return _this;
    }

    _createClass(ContentBodyIframe, [{
        key: 'componentDidMount',
        value: function componentDidMount() {
            var _this2 = this;

            this.receiveMessage = function (event) {
                var iframeId = event.data.iframeId;


                if (!iframeId || iframeId !== _this2.iframeId) {
                    return;
                }

                var height = event.data.height;


                _this2.setState({
                    height: height + 'px'
                });
            };

            (0, _window.addEventListener)('message', this.receiveMessage);
        }
    }, {
        key: 'componentWillUnmount',
        value: function componentWillUnmount() {
            (0, _window.removeEventListener)('message', this.receiveMessage);
        }
    }, {
        key: 'render',
        value: function render() {
            var _props = this.props;
            var content = _props.content;
            var baseUrl = _props.baseUrl;
            var onContentLoaded = _props.onContentLoaded;
            var contextPath = this.props.contextPath;
            var height = this.state.height;


            if (typeof contextPath === 'undefined') {
                contextPath = DEFAULT_CONTEXT_PATH;
            }

            var contentUrl = '' + baseUrl + contextPath + '/content-only/viewpage.action?pageId=' + content.id + '&iframeId=' + this.iframeId;

            return _react2.default.createElement('iframe', {
                src: contentUrl,
                border: '0',
                style: { border: 0, width: '100%', height: height },
                onLoad: onContentLoaded
            });
        }
    }]);

    return ContentBodyIframe;
}(_react.Component);

exports.default = ContentBodyIframe;


ContentBodyIframe.displayName = 'ContentBodyIframe';

ContentBodyIframe.defaultProps = {
    baseUrl: ''
};

ContentBodyIframe.propTypes = {
    /**
     * The ID of the content to render.
     */
    contentId: _react.PropTypes.string,
    /**
     * Host of confluence instance for the iframe src attribute
     */
    baseUrl: _react.PropTypes.string,
    /**
     * Confluence instance context path
     */
    contextPath: _react.PropTypes.string,
    /**
     * Callback when iframe finishes loading.
     */
    onContentLoaded: _react.PropTypes.func
};